﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FirstProjectIndigo.Data
{
    public class BillingInformation
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter a billing info name")]
        public string Name { get; set; }
        public string Adress { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string BTWNumber { get; set; }
        public string Email { get; set; }
        public bool ReceiveByMail { get; set; }
        public User User { get; set; }
        public int? UserId { get; set; }
    }
}
