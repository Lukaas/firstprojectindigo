﻿using FirstProjectIndigo.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstProjectIndigo.Services
{
    public class UserService
    {
        private readonly IDbContextFactory<UserManagerDbContext> _contextFactory;

        public UserService(IDbContextFactory<UserManagerDbContext> contextFactory)
        {
            this._contextFactory = contextFactory;
        }
        public async Task<List<User>> GetUsersAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Users.Include(u => u.BillingInformations).ToListAsync();
        }
        public Task DeleteUser(User toRemoveUser)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Users.Remove(toRemoveUser);
                context.SaveChanges();
            }
            return Task.CompletedTask;
        }
        public async Task<User> AddUserAsync(User toAddUser)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                await context.Users.AddAsync(toAddUser);
                context.SaveChanges();
            }
            return await Task.FromResult(toAddUser);
        }
    }
}
