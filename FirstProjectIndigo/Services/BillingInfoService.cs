﻿using FirstProjectIndigo.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstProjectIndigo.Services
{
    public class BillingInfoService
    {
        private readonly IDbContextFactory<UserManagerDbContext> _contextFactory;

        public BillingInfoService(IDbContextFactory<UserManagerDbContext> contextFactory)
        {
            this._contextFactory = contextFactory;
        }
        public Task DeleteBillingInfo(BillingInformation toRemoveBillingInfo)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                context.BillingInformations.Remove(toRemoveBillingInfo);
                context.SaveChanges();
            }
            return Task.CompletedTask;
        }
        public async Task<BillingInformation> AddBillingAsync(BillingInformation toAddBilling)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                await context.BillingInformations.AddAsync(toAddBilling);
                context.SaveChanges();
            }
            return await Task.FromResult(toAddBilling);
        }
    }
}
